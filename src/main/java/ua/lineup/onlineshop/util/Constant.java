package ua.lineup.onlineshop.util;

public class Constant {

    public static final String API_KEY = "NmE0ZTBhM2U0YWIzMmVmZDAxYjMzMTk3Y2UwY2ZhOTc0Mjk5NmM1NjVhNTUwMDU5ZTUwYjZlZmIyMTMyNDk2NA==";

    public static final String API_KEY_NAME = "EASYTRANSAC-API-KEY";

    public static final String CONTENT_TYPE = "application/x-www-form-urlencoded";

    public static final String CONTENT_TYPE_NAME = "Content-Type";

    public static final String PARAM_AMOUNT = "Amount";

    public static final String PARAM_CARD_CVV = "CardCVV";

    public static final String PARAM_CARD_MONTH = "CardMonth";

    public static final String PARAM_CARD_NUNMER = "CardNumber";

    public static final String PARAM_CARD_YEAR = "CardYear";

    public static final String PARAM_CLIENT_IP = "ClientIp";

    public static final String PARAM_EMAIL = "Email";

    public static final String PARAM_UID = "Uid";

    public static final String PARAM_SIGNATURE = "Signature";

    public static final String PARAM_URL = "https://www.easytransac.com/api/payment/direct";

    public static final String RESPONSE_PARAM_RESULT = "Result";

    public static final String RESPONSE_PARSM_MESSAGE = "Message";

    public static final String RESPONSE_PARAM_ERROR = "Error";
}
