package ua.lineup.onlineshop.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.lineup.onlineshop.model.Item;
import ua.lineup.onlineshop.repository.ItemRepository;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
@Transactional
public class ItemService {

    private final ItemRepository itemRepository;

    @Autowired
    public ItemService(ItemRepository itemRepository) {
        this.itemRepository = itemRepository;
    }

    public List<Item> getAll() {
        return itemRepository.findAll();
    }

    public Item getById(Integer itemId) {
        return itemRepository.findById(itemId).orElseThrow(() -> new EntityNotFoundException("Not found user by id"));
    }
}
