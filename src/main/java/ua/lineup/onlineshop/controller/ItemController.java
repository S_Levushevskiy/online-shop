package ua.lineup.onlineshop.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ua.lineup.onlineshop.model.Item;
import ua.lineup.onlineshop.service.ItemService;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class ItemController {

    private final ItemService itemService;

    @Autowired
    public ItemController(ItemService itemService) {
        this.itemService = itemService;
    }

    @GetMapping("/items")
    public String showPage(Model model) {
        List<Item> items = itemService.getAll();
        model.addAttribute("itemList", items);
        return "itemCatalog";
    }

    @PostMapping("/items")
    public String showCatalog(Model model) {
        List<Item> items = itemService.getAll();
        model.addAttribute("itemList", items);
        return "itemCatalog";
    }

}
