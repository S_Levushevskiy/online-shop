package ua.lineup.onlineshop.controller;

import net.minidev.json.JSONObject;
import net.minidev.json.parser.JSONParser;
import net.minidev.json.parser.ParseException;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ua.lineup.onlineshop.controller.response.ItemResponse;
import ua.lineup.onlineshop.service.ItemService;
import ua.lineup.onlineshop.util.Constant;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Controller
public class PaymentController {

    private final ItemService itemService;

    @Autowired
    public PaymentController(ItemService itemService) {
        this.itemService = itemService;
    }

    @GetMapping("/payment")
    public String getPaymentForm(Model model, HttpServletRequest httpServletRequest) {
        Integer itemId = Integer.parseInt(httpServletRequest.getParameter("itemId"));
        ItemResponse itemResponse = new ItemResponse(itemService.getById(itemId));
        model.addAttribute("item", itemResponse);
        return "item";
    }

    @GetMapping("/result")
    public String resultForm(Model model, HttpServletRequest httpServletRequest){
        HttpSession session = httpServletRequest.getSession();
        ItemResponse itemResponse = (ItemResponse)session.getAttribute("item");
        model.addAttribute("item", itemResponse);
        model.addAttribute("flagError",session.getAttribute("flagError"));
        return "payment";
    }

    @PostMapping("/payment")
    public String paymentForm(Model model,@RequestParam Integer itemId,@RequestParam Integer count){
        ItemResponse itemResponse = new ItemResponse(itemService.getById(itemId));
        itemResponse.setCount(count);
        model.addAttribute("item", itemResponse);
        return "payment";
    }


    @PostMapping("/send")
    public String createPayment(Model model, HttpServletRequest httpServletRequest) throws ParseException {
        String content = sendPayment(httpServletRequest);

        Integer itemId = Integer.parseInt(httpServletRequest.getParameter("itemId"));
        ItemResponse itemResponse = new ItemResponse(itemService.getById(itemId));

        Boolean flagError = false;
        Object obj = new JSONParser().parse(content);
        JSONObject jo = (JSONObject) obj;
        String message, confirmUrl="";
        JSONObject result = (JSONObject) jo.get(Constant.RESPONSE_PARAM_RESULT);
        if (result != null) {
            message = (String) result.get(Constant.RESPONSE_PARSM_MESSAGE);
            confirmUrl = (String) result.get("3DSecureUrl");
        } else {
            flagError = true;
            message = (String) jo.get(Constant.RESPONSE_PARAM_ERROR);
        }
        itemResponse.setMessage(message);
        itemResponse.setConfirmUrl(confirmUrl);
        itemResponse.setCount(Integer.parseInt(httpServletRequest.getParameter("count")));
        model.addAttribute("item", itemResponse);
        HttpSession session = httpServletRequest.getSession();
        session.setAttribute("flagError", flagError);
        session.setAttribute("item", itemResponse);
        if(flagError==true){
            return "redirect:/result#message";
        }
        return "redirect:"+confirmUrl;
    }

    @GetMapping("/response")
    public String response(Model model, HttpServletRequest request) {
        HttpSession session = request.getSession();
        ItemResponse itemResponse = (ItemResponse) session.getAttribute("item");
        model.addAttribute("item", itemResponse);
        return "response";
    }


    private String sendPayment(HttpServletRequest httpServletRequest) {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        String returnUrl =String.format("%s://%s:%d/%s",httpServletRequest.getScheme(),  httpServletRequest.getServerName(),
                httpServletRequest.getServerPort(),"items");
        returnUrl+="#message";
        HttpPost httpPost = new HttpPost(Constant.PARAM_URL);
        String amount = httpServletRequest.getParameter(Constant.PARAM_AMOUNT);
        if (amount.indexOf(".") + 2 == amount.length()) {
            amount = amount.replace(".", "") + "0";
        } else {
            amount = amount.replace(".", "");
        }
        List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("3DS", "yes"));
        params.add(new BasicNameValuePair(Constant.PARAM_AMOUNT, amount));
        params.add(new BasicNameValuePair(Constant.PARAM_CARD_CVV, httpServletRequest.getParameter(Constant.PARAM_CARD_CVV)));
        params.add(new BasicNameValuePair(Constant.PARAM_CARD_MONTH, httpServletRequest.getParameter(Constant.PARAM_CARD_MONTH)));
        params.add(new BasicNameValuePair(Constant.PARAM_CARD_NUNMER, httpServletRequest.getParameter(Constant.PARAM_CARD_NUNMER).replaceAll(" ", "")));
        params.add(new BasicNameValuePair(Constant.PARAM_CARD_YEAR, httpServletRequest.getParameter(Constant.PARAM_CARD_YEAR)));
        params.add(new BasicNameValuePair(Constant.PARAM_CLIENT_IP, httpServletRequest.getRemoteAddr()));
        params.add(new BasicNameValuePair(Constant.PARAM_EMAIL, httpServletRequest.getParameter(Constant.PARAM_EMAIL)));
        params.add(new BasicNameValuePair("ReturnUrl", returnUrl));
        String uid = DigestUtils.sha1Hex(httpServletRequest.getParameter(Constant.PARAM_EMAIL));
        params.add(new BasicNameValuePair(Constant.PARAM_UID, uid));
        String signature = "";
        for (int i = 0; i < params.size(); i++) {
            signature += params.get(i).getValue() + "$";
        }

        signature += Constant.API_KEY;
        signature = DigestUtils.sha1Hex(signature);
        params.add(new BasicNameValuePair(Constant.PARAM_SIGNATURE, signature));
        httpPost.setHeader(Constant.CONTENT_TYPE_NAME, Constant.CONTENT_TYPE);
        httpPost.setHeader(Constant.API_KEY_NAME, Constant.API_KEY);

        String content = "";
        try {
            httpPost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
            HttpResponse response = httpClient.execute(httpPost);
            HttpEntity entity = response.getEntity();
            content = EntityUtils.toString(entity);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return content;
    }
}
