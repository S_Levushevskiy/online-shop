package ua.lineup.onlineshop.controller.response;

import lombok.Data;
import lombok.EqualsAndHashCode;
import ua.lineup.onlineshop.model.Item;

import java.io.Serializable;

@Data
@EqualsAndHashCode
public class ItemResponse implements Serializable{

    private Integer id;

    private String name;

    private String description;

    private String img;

    private Double price;

    private Integer count;

    private String message;

    private String confirmUrl;

    public void setCount(Integer count) {
        this.count = count;
        this.price *= count;
    }

    public ItemResponse(Item item) {
        this.id = item.getId();
        this.name = item.getName();
        this.description = item.getDescription();
        this.img = item.getImg();
        this.price = item.getPrice();
    }
}
