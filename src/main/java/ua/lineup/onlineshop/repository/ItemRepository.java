package ua.lineup.onlineshop.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ua.lineup.onlineshop.model.Item;

@Repository
public interface ItemRepository extends JpaRepository<Item,Integer>{

}
