package ua.lineup.onlineshop.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "item")
@Data
@EqualsAndHashCode
public class Item implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @GenericGenerator(name = "native", strategy = "native")
    private Integer id;

    private String name;

    private String description;

    private String img;

    @Column(columnDefinition = "DOUBLE(7.2)")
    private Double price;
}
